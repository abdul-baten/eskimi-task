import { Component, OnInit, HostListener, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { TitleService } from 'src/app/services/title.service';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Photos } from 'src/app/models/photos';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  /**
   * Title for the Project
   */
  public title: string = 'Eskimi Task';
  private photosSubscription: Subscription;
  public displayedColumns: string[] = ['id', 'albumId', 'title', 'thumbnailUrl'];
  public dataSource: MatTableDataSource<Photos[]>;
  public clickedRow: Photos;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  /**
   * Constructor for AppComponent Class
   * @param titleService
   */

  constructor(private titleService: TitleService, private route: ActivatedRoute) {
  }

  /**
   * Angular OnInit LifeCycle
   */
  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.getPhotos();
  }

  getPhotos(): void {
    this.dataSource = new MatTableDataSource<Photos[]>(this.route.snapshot.data.photos);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onRowClicked(row: Photos): void {
    this.clickedRow = row as Photos;
  }

  @HostListener('window:beforeunload')
  ngOnDestroy(): void {
    if (this.photosSubscription) {
      this.photosSubscription.unsubscribe();
    }
  }

}
