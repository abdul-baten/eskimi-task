import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Photos } from '../models/photos';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, shareReplay, retry } from 'rxjs/operators';

@Injectable()
export class HomeResolve implements Resolve<Photos[]> {

  private photosJsonApiUrl: string = environment.jsonApiUrl;

  constructor(private httpClient: HttpClient) { }

  resolve(): Observable<Photos[]> {
    return this.httpClient.get<Photos[]>(this.photosJsonApiUrl).pipe(
      map(response => response as Photos[]),
      shareReplay(),
      retry(2),
    )
  }
}
