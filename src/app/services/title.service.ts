import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  /* Constructor for TitleService Class */
  constructor(private title: Title) { }

  /**
   * Public method to set Page title
   * @param titleString -> type String
   * Return type Void
   */
  setTitle(titleString: string): void {
    this.title.setTitle(titleString);
  }
}
